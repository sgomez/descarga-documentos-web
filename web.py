#El programa debe descargar documentos web

import urllib.request


class Robot:

    def __init__(self, url):
        self.url = url
        self.downloaded = False

    #Descargar el documento de la url sino lo ha hecho antes
    def retrieve(self):
        if not self.downloaded:
            print("\n")
            print("Descargando url...\n")
            d = urllib.request.urlopen(self.url)
            self.content = d.read().decode('utf-8')
            self.downloaded = True
            print("Url descargada\n")

    #Muestra por pantalla el contenido descragado
    def show(self):
        print(self.content())

    # Devuelve una cadena de caracteres del documento descargado
    def content(self):
        self.retrieve()
        return self.content


#Proporciona una cache de documentos
class Cache:

    def __init__(self):
        self.cache = {}
        self.download = False

    #Descarga el documento si no se hizo antes
    def retrieve(self, url):
        while url not in self.cache:
            self.download = False
            robot = Robot(url = url)
            self.cache[url] = robot
            self.download = True

    #Muetra por panatlla el contenido del documento
    def show(self, url):
        print(self.content(url))

    #Muestra listado de todas las urls
    def show_all(self):
        for url in self.cache:
            print("La url mostrada es: {}".format(url))

    #Devuelve cadena de caracteres con el contenido del documento
    def content(self, url):
        self.retrieve(url)
        return self.cache[url].content()


if __name__ == '__main__':
    print("---------ROBOT---------")
    robot_ = Robot('https://www.vogue.es/')
    print(robot_.url)
    robot_.show()
    robot_.retrieve()
    robot_.retrieve()

    print("---------CACHE---------")
    cache_ = Cache()
    print("Url 1: {}".format(robot_.url))
    cache_.retrieve('https://www.vogue.es/')
    print("Url 2: {}".format(robot_.url))
    cache_.show('https://www.ifema.es/mbfw-madrid')
    cache_.show_all()
